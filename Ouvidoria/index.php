<?php
    session_start();
?>

<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
   
        <title>Ouvidoria</title>
    </head>

    <body>

    <?php 
        include('navbar.php');
        include('destroy.php');
    ?>
    <style>
            div#cadastro{
                display: none;
            }
    </style>
    <script type="text/javascript">
    function displaySwap(){
            document.getElementById("botao_seletor").style.display == 'none';
            document.getElementById("cadastro").style.display == 'block';
            
    }
    </script>

    <div class="container-fluid" id="botao_seletor">
        <div class="col-md-6 col-md-offset-3 painel-login">
            <div class="form-group col-md-12">
                <input type="submit" class="btn btn-large btn-block btn-success" value="Consultar Reclamação" onclick="displaySwap();"></button>
            </div>
            <div class="form-group col-md-12">
                <input type="submit" class="btn btn-large btn-block btn-success" value="Abrir Reclamação"></button>
            </div>         
        </div>
    </div>
    


    <!-- Pagina cadastro -->
    <div class="container-fluid" id="cadastro">
        <div class="col-md-6 col-md-offset-3 painel-login">
            
            <!--method="POST" para se usar no form-->
            <!-- Formulário de Ouvidoria -->
            <form>
                <h2 class="text-center"> Área de Cadastro </h2>
                <div class="form-group col-md-12">
                    <label for="usuario">Usuário:</label>
                    <input name="usuario" class="form-control" type="text" required>
                </div>

                <div class="form-group col-md-12">
                    <label for="senha">Senha:</label>
                    <input name="senha" class="form-control" type="password" required>
                </div>

                <div class="form-group col-md-12">
                    <input type="submit" class="btn btn-large btn-block btn-success" value="Entrar"></button>
                </div>
                
                <div class="col-md-12 text-center">   
                    <?php
                        if(isset($_POST["erro"])) {
                            echo '<span class="alert alert-danger col-md-12">' . $_POST["erro"] . '</span>';
                        }
                    ?>
                </div>
            
            </form>
        </div>
    </div>
    
    </body>

</html>